var $table = $('#table-list-winner')

function buildTable($el) {
$el.bootstrapTable('destroy').bootstrapTable({
  showFullscreen: true,
  search: false,
  stickyHeader: true,
  theadClasses: undefined,
  toolbar:false
})
}

$(function() {
buildTable($table, 20, 50)
})